package com.sample.test

//import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
//import org.testng.Assert as Assert

class TestFriday {
  /**
  * The step definitions below match with Katalon sample Gherkin steps
  */
 String today ="";
 String actual_Answer="";
 def is_it_friday(String today){
  if (today.equals("Friday") ) {
   return "TGIF";
  } else {
   return "Nope";
  }
 }
 @Given("today is (.*)")
 def Today_Is(String givenDay ) {
  today = givenDay;
 }

 @When("I ask whether it's Friday yet")
 def Whether_Is_It_Friday() {
  actual_Answer=is_it_friday(today);
 }

 @Then("I should be told (.*)")
 def I_verify_the_status_in_step(String expectedAnswer) {
   //Assert.assertEquals(actual_Answer, expectedAnswer)
   assert actual_Answer == expectedAnswer;
 }
}