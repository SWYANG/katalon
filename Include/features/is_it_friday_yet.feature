Feature: Is it Friday yet again?
  Everybody wants to know when it's Friday

  Scenario Outline: Today is or is not Friday
    Given today is <day>
    When I ask whether it's Friday yet
    Then I should be told <answer>

  Examples:
    | day | answer |
    | Friday | TGIF |
    | Sunday | Nope |
    | Monday | Nope |
    | Tuesday | Nope |
    | anything else! | Nope |